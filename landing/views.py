from django.db import connection
from django.shortcuts import render
import locale

from .forms import SearchForm

# Create your views here.
def index(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to fabelio")
    
    if "last_item" not in request.session.keys():
        request.session["last_item"] = ""
        request.session["visit"] = -1

    if request.method == "POST":
        form = SearchForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["data"]
            query = "select * from item where LOWER(name)=LOWER('"+name+"')"

            cursor.execute(query)
            result = cursor.fetchall()

            if(result == []):
                response["items"] = None
            else:
                response["items"] = list(result[0])

                request.session["last_item"] = result[0][6]
                request.session["visit"] = 0

    else:
        last_item = request.session["last_item"]
        print(last_item)

        if(last_item == ''):
            cursor.execute("select * from item")
        else:
            cursor.execute("select * from item where LOWER(type)=LOWER('"+last_item+"')")

        idx = request.session["visit"] = request.session["visit"]+1

        result = cursor.fetchall()
        response["items"] = list(result[idx % len(result)])

    response["form"] = SearchForm()

    if(response["items"] != None):
        response["items"][1] = rupiah_format(response["items"][1])
        
    return render(request, "main-pages/index.html", response)

def rupiah_format(num):
    thousands_separator = "."

    fractional_separator = ","
    currency = "Rp{:,.2f}".format(num)

    if thousands_separator == ".":
        main_currency, fractional_currency = currency.split(".")[0], currency.split(".")[1]
        new_main_currency = main_currency.replace(",", ".")
        currency = new_main_currency + fractional_separator + fractional_currency

    return currency